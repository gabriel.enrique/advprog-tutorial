package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import java.util.ArrayList;

public class ChainSpell implements Spell {
    
    private ArrayList<Spell> spells;

    public ChainSpell(ArrayList<Spell> spells) {
        this.spells = spells;
    }

    @Override
    public void cast() {
        // Cast all spells in order
        for (Spell spell : spells) {
            spell.cast();
        }
    }

    @Override
    public void undo() {
        // Undo all spells in reverse order
        for (int i = spells.size()-1; i >= 0; i--) {
            spells.get(i).undo();
        }
    }

    @Override
    public String spellName() {
        return "ChainSpell";
    }
}
