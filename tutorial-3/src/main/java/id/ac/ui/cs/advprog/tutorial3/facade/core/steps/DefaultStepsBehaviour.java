package id.ac.ui.cs.advprog.tutorial3.facade.core.steps;

import id.ac.ui.cs.advprog.tutorial3.facade.core.adapter.CodexAdapter;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.CodexTranslator;
import id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.AbyssalTransformation;
import id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.CaesarTransformation;
import id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.CelestialTransformation;

public class DefaultStepsBehaviour extends StepsBehaviour {

    public DefaultStepsBehaviour() {
        super();
    }

    @Override
    protected void initialize() {
        steps.add(new CelestialTransformation());
        steps.add(new AbyssalTransformation());
        steps.add(new CodexAdapter(new CodexTranslator()));
        steps.add(new CaesarTransformation());
    }
}
