package id.ac.ui.cs.advprog.tutorial3.adapter.service;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.Bow;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.BowAdapter;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.SpellbookAdapter;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.BowRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.LogRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.SpellbookRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.WeaponRepository;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

// Modify this class as you see fit~
@Service
public class WeaponServiceImpl implements WeaponService {

    // feel free to include more repositories if you think it might help :)
    @Autowired
    private WeaponRepository weaponRepository;

    @Autowired
    private BowRepository bowRepository;

    @Autowired
    private SpellbookRepository spellbookRepository;

    @Autowired
    private LogRepository logRepository;

    private boolean isAdapted = false;

    // Adapt all Bow and Spellbook into Weapon
    public void adapt() {
        if (isAdapted) return; // Only run once

        for (Bow bow : bowRepository.findAll()) {
            Weapon adaptedBow = new BowAdapter(bow);
            weaponRepository.save(adaptedBow);
        }

        for (Spellbook spellbook : spellbookRepository.findAll()) {
            Weapon adaptedSpellbook = new SpellbookAdapter(spellbook);
            weaponRepository.save(adaptedSpellbook);
        }

        this.isAdapted = true;
    }

    @Override
    public List<Weapon> findAll() {
        adapt();
        return weaponRepository.findAll();
    }

    @Override
    public void attackWithWeapon(String weaponName, int attackType) {
        if (attackType != 1 && attackType != 2) return; // 1 for Normal Attack and 2 for Charged Attack

        Weapon weapon = weaponRepository.findByAlias(weaponName);
        String log = String.format("%s attacked with %s (%s): %s",
                                        weapon.getHolderName(),
                                        weapon.getName(),
                                        attackType == 1 ? "Normal Attack" : "Charged Attack",
                                        attackType == 1 ? weapon.normalAttack() : weapon.chargedAttack());

        weaponRepository.save(weapon);
        logRepository.addLog(log);
    }

    @Override
    public List<String> getAllLogs() {
        return logRepository.findAll();
    }
}
