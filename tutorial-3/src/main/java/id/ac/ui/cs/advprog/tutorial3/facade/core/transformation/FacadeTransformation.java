package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;
import id.ac.ui.cs.advprog.tutorial3.facade.core.steps.DefaultStepsBehaviour;
import id.ac.ui.cs.advprog.tutorial3.facade.core.steps.StepsBehaviour;

public class FacadeTransformation implements Transformation {

    private StepsBehaviour stepsBehaviour;

    public FacadeTransformation() {
        this(new DefaultStepsBehaviour());
    }

    public FacadeTransformation(StepsBehaviour stepsBehaviour) {
        this.stepsBehaviour = stepsBehaviour;
    }

    public void setStepsBehaviour(StepsBehaviour behaviour) {
        this.stepsBehaviour = behaviour;
    }

    public StepsBehaviour getStepsBehaviour() {
        return stepsBehaviour;
    }

    public Spell encode(Spell spell) {
        Spell result = spell;

        for (Transformation transformation : stepsBehaviour.getEncodeSteps())
            result = transformation.encode(result);

        return result;
    }

    public Spell decode(Spell spell) {
        Spell result = spell;

        for (Transformation transformation : stepsBehaviour.getDecodeSteps())
            result = transformation.decode(result);

        return result;
    }
}
