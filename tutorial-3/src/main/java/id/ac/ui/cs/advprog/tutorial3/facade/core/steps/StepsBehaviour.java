package id.ac.ui.cs.advprog.tutorial3.facade.core.steps;

import id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.Transformation;

import java.util.ArrayList;
import java.util.List;

public abstract class StepsBehaviour {

    protected List<Transformation> steps;

    public StepsBehaviour() {
        this.steps = new ArrayList<>();
        initialize();
    }

    protected abstract void initialize();

    public List<Transformation> getEncodeSteps() {
        return steps;
    }

    public List<Transformation> getDecodeSteps() {
        List<Transformation> decodeSteps = new ArrayList<>();

        // Reverse of the encode steps
        for (int i = steps.size() - 1; i >= 0; i--)
            decodeSteps.add(steps.get(i));

        return decodeSteps;
    }
}
