package id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon;

public class SeawardPride implements Weapon {

    private String holderName;

    public SeawardPride(String holderName) {
        this.holderName = holderName;
    }

    @Override
    public String normalAttack() {
        return String.format("%s normal attack", getName());
    }

    @Override
    public String chargedAttack() {
        return "Retribution!";
    }

    @Override
    public String getName() {
        return "Seaward Pride";
    }

    @Override
    public String getHolderName() { return holderName; }
}
