package id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon;

public class FesteringGreed implements Weapon {

    private String holderName;

    public FesteringGreed(String holderName) {
        this.holderName = holderName;
    }

    @Override
    public String normalAttack() {
        return String.format("%s normal attack", getName());
    }

    @Override
    public String chargedAttack() {
        return "Sorya! Ha!";
    }

    @Override
    public String getName() {
        return "Festering Greed";
    }

    @Override
    public String getHolderName() {
        return holderName; 
    }
}
