package id.ac.ui.cs.advprog.tutorial3.facade.core.adapter;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.AlphaCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.RunicCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.CodexTranslator;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;
import id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.Transformation;

public class CodexAdapter implements Transformation {

    private CodexTranslator translator;

    public CodexAdapter(CodexTranslator translator) {
        this.translator = translator;
    }

    @Override
    public Spell encode(Spell spell) {
        return translator.translate(spell, RunicCodex.getInstance());
    }

    @Override
    public Spell decode(Spell spell) {
        return translator.translate(spell, AlphaCodex.getInstance());
    }
}
