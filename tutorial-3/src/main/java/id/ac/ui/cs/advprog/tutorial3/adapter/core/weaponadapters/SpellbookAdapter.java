package id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;

public class SpellbookAdapter implements Weapon {

    private Spellbook spellbook;
    private boolean largeSpellReady = true;

    public SpellbookAdapter(Spellbook realSpellbook) {
        this.spellbook = realSpellbook;
    }

    @Override
    public String normalAttack() {
        this.largeSpellReady = true;
        return this.spellbook.smallSpell();
    }

    @Override
    public String chargedAttack() {
        // Cannot cast large spell twice in a row
        if (largeSpellReady) {
            this.largeSpellReady = false;
            return this.spellbook.largeSpell();
        } else {
            return "Not enough energy!";
        }
    }

    @Override
    public String getName() {
        return this.spellbook.getName();
    }

    @Override
    public String getHolderName() {
        return this.spellbook.getHolderName();
    }
}
