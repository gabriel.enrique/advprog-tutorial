package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;

public class CaesarTransformation implements Transformation {

    private int shiftAmount;

    public CaesarTransformation() {
        this(19);
    }

    public CaesarTransformation(int shiftAmount) {
        this.shiftAmount = shiftAmount;
    }

    @Override
    public Spell encode(Spell spell) {
        return transform(spell, "f");
    }

    @Override
    public Spell decode(Spell spell) {
        return transform(spell, "b");
    }

    // Caesar Cipher
    private Spell transform(Spell spell, String direction) {
        String text = spell.getText();
        String result = "";

        for (int i = 0; i < text.length(); i++)
            result += shift(text.charAt(i), direction);

        return new Spell(result, spell.getCodex());
    }

    private String shift(char c, String direction) {
        switch (direction) {
            case "f":
                return Character.toString((char) (c + shiftAmount));
            case "b":
                return Character.toString((char) (c - shiftAmount));
            default:
                return null;
        }
    }
}
