package id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.Bow;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;

public class BowAdapter implements Weapon {

    private Bow bow;
    private boolean isAimShot = false;

    public BowAdapter(Bow bow) {
        this.bow = bow;
    }

    @Override
    public String normalAttack() {
        return this.bow.shootArrow(isAimShot);
    }

    @Override
    public String chargedAttack() {
        // Toggle between normal and aim shot
        if (isAimShot) {
            this.isAimShot = false;
            return "Going back to normal attack mode";
        } else {
            this.isAimShot = true;
            return "Aim shot ready!";
        }
    }

    @Override
    public String getName() {
        return this.bow.getName();
    }

    @Override
    public String getHolderName() {
        return this.bow.getHolderName();
    }
}
