package id.ac.ui.cs.advprog.tutorial3.facade.core.steps;

import id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.AbyssalTransformation;
import id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.CelestialTransformation;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class StepsBehaviourTest {

    private Class<?> stepsBehaviourClass;

    @BeforeEach
    public void setUp() throws Exception {
        this.stepsBehaviourClass = Class.forName("id.ac.ui.cs.advprog.tutorial3.facade.core.steps.StepsBehaviour");
    }

    @Test
    public void testStepsBehaviourIsPublicAbstractClass() throws Exception {
        int classModifiers = stepsBehaviourClass.getModifiers();

        assertTrue(Modifier.isPublic(classModifiers));
        assertTrue(Modifier.isAbstract(classModifiers));
    }

    @Test
    public void testStepsBehaviourHasProtectedAbstractInitializeMethod() throws Exception {
        Method getCharacters = stepsBehaviourClass.getDeclaredMethod("initialize");
        int methodModifiers = getCharacters.getModifiers();
        assertTrue(Modifier.isProtected(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
    }

    @Test
    public void testStepsBehaviourHasGetEncodeStepsMethod() throws Exception {
        Method getCharacters = stepsBehaviourClass.getDeclaredMethod("getEncodeSteps");
        int methodModifiers = getCharacters.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testStepsBehaviourHasGetDecodeStepsMethod() throws Exception {
        Method getCharacters = stepsBehaviourClass.getDeclaredMethod("getDecodeSteps");
        int methodModifiers = getCharacters.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testDecodeStepsIsEncodeStepsReversed() throws Exception {
        StepsBehaviour stepsBehaviour = new StepsBehaviour() {
            @Override
            protected void initialize() {
                steps.add(new AbyssalTransformation());
                steps.add(new CelestialTransformation());
            }
        };

        assertTrue(stepsBehaviour.getEncodeSteps().get(0) == stepsBehaviour.getDecodeSteps().get(1));
        assertTrue(stepsBehaviour.getEncodeSteps().get(1) == stepsBehaviour.getDecodeSteps().get(0));
    }
}
