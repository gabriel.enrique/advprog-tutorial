package id.ac.ui.cs.advprog.tutorial3.facade.core.steps;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class DefaultStepsBehaviourTest {

    private Class<?> defaultStepsBehaviourClass;

    @BeforeEach
    public void setUp() throws Exception {
        this.defaultStepsBehaviourClass = Class.forName("id.ac.ui.cs.advprog.tutorial3.facade.core.steps.DefaultStepsBehaviour");
    }

    @Test
    public void testDefaultStepsBehaviourIsPublicClass() throws Exception {
        int classModifiers = defaultStepsBehaviourClass.getModifiers();

        assertTrue(Modifier.isPublic(classModifiers));
    }

    @Test
    public void testDefaultStepsBehaviourExtendsStepsBehaviourClass() throws Exception {
        DefaultStepsBehaviour defaultStepsBehaviour = new DefaultStepsBehaviour();
        assertTrue(defaultStepsBehaviour instanceof StepsBehaviour);
    }

    @Test
    public void testDefaultStepsBehaviourHasInitializeMethod() throws Exception {
        Method getCharacters = defaultStepsBehaviourClass.getDeclaredMethod("initialize");
        int methodModifiers = getCharacters.getModifiers();
        assertTrue(Modifier.isProtected(methodModifiers));
        assertTrue(!Modifier.isAbstract(methodModifiers));
    }
}
