package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.AlphaCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.RunicCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.*;

public class CaesarTransformationTest {
    private Class<?> caesarClass;
    private CaesarTransformation caesarTransformation;

    @BeforeEach
    public void setUp() throws Exception {
        this.caesarClass = Class.forName("id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.CaesarTransformation");
        this.caesarTransformation = new CaesarTransformation();
    }

    @Test
    public void testCaesarTransformationHasEncodeMethod() throws Exception {
        Method translate = caesarClass.getDeclaredMethod("encode", Spell.class);
        int methodModifiers = translate.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testCaesarTransformationEncodeResult() throws Exception {
        Spell spell = new Spell("ABC", AlphaCodex.getInstance());
        String caesarTransformationResult = caesarTransformation.encode(spell).getText();
        assertEquals("TUV", caesarTransformationResult);
    }

    @Test
    public void testCaesarTransformationEncodeCustomKeyResult() throws Exception {
        Spell spell = new Spell("CaesarSalad", AlphaCodex.getInstance());
        String caesarTransformationResult = new CaesarTransformation(1).encode(spell).getText();
        assertEquals("DbftbsTbmbe", caesarTransformationResult);
    }

    @Test
    public void testAbyssalHasDecodeMethod() throws Exception {
        Method translate = caesarClass.getDeclaredMethod("decode", Spell.class);
        int methodModifiers = translate.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testCaesarTransformationDecodeResult() throws Exception {
        Spell spell = new Spell("TUV", RunicCodex.getInstance());
        String caesarTransformationResult = caesarTransformation.decode(spell).getText();
        assertEquals("ABC", caesarTransformationResult);
    }

    @Test
    public void testCaesarTransformationDecodeCustomKeyResult() throws Exception {
        Spell spell = new Spell("DbftbsTbmbe", AlphaCodex.getInstance());
        String caesarTransformationResult = new CaesarTransformation(1).decode(spell).getText();
        assertEquals("CaesarSalad", caesarTransformationResult);
    }
}
