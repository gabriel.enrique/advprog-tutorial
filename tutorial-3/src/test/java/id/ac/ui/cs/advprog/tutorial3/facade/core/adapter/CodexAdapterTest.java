package id.ac.ui.cs.advprog.tutorial3.facade.core.adapter;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.AlphaCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.RunicCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.CodexTranslator;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class CodexAdapterTest {
    private CodexAdapter codexAdapter;
    private CodexTranslator codexTranslator;

    @BeforeEach
    public void setUp() {
        this.codexTranslator = new CodexTranslator();
        this.codexAdapter = new CodexAdapter(codexTranslator);
    }

    @Test
    public void testCodexAdapterEncodeResult() throws Exception {
        Spell spell = new Spell("Safira and I went to a blacksmith to forge our sword", AlphaCodex.getInstance());
        String codexTranslatorResult = codexTranslator.translate(spell, RunicCodex.getInstance()).getText();
        String codexAdapterResult = codexAdapter.encode(spell).getText();
        assertEquals(codexTranslatorResult, codexAdapterResult);
    }

    @Test
    public void testCodexAdapterDecodeResult() throws Exception {
        Spell spell = new Spell("eJcnBJ_JZz_._DxZM_MX_J_KsJLaNdnMb_MX_cXBvx_XAB_NDXBz", RunicCodex.getInstance());
        String codexTranslatorResult = codexTranslator.translate(spell, AlphaCodex.getInstance()).getText();
        String codexAdapterResult = codexAdapter.decode(spell).getText();
        assertEquals(codexTranslatorResult, codexAdapterResult);
    }
}
