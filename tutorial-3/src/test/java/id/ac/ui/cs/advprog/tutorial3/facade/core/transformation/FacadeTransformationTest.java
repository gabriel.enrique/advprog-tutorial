package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;
import id.ac.ui.cs.advprog.tutorial3.facade.core.steps.DefaultStepsBehaviour;
import id.ac.ui.cs.advprog.tutorial3.facade.core.steps.StepsBehaviour;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class FacadeTransformationTest {

    private Class<?> facadeTransformationClass;

    @BeforeEach
    public void setUp() throws Exception {
        this.facadeTransformationClass = Class.forName("id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.FacadeTransformation");
    }

    @Test
    public void testTransformatorIsAPublicClass() {
        int classModifiers = facadeTransformationClass.getModifiers();
        assertTrue(Modifier.isPublic(classModifiers));
    }

    @Test
    public void testTransformatorUseDefaultStepsBehaviourIfNoArgumentOnConstructor() {
        FacadeTransformation facadeTransformation = new FacadeTransformation();
        assertTrue(facadeTransformation.getStepsBehaviour() instanceof DefaultStepsBehaviour);
    }

    @Test
    public void testTransformatorHasSetStepsBehaviourMethod() throws Exception {
        Method translate = facadeTransformationClass.getDeclaredMethod("setStepsBehaviour", StepsBehaviour.class);
        int methodModifiers = translate.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testTransformatorHasGetStepsBehaviourMethod() throws Exception {
        Method translate = facadeTransformationClass.getDeclaredMethod("getStepsBehaviour");
        int methodModifiers = translate.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testTransformatorHasEncodeMethod() throws Exception {
        Method translate = facadeTransformationClass.getDeclaredMethod("encode", Spell.class);
        int methodModifiers = translate.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testTransformatorHasDecodeMethod() throws Exception {
        Method translate = facadeTransformationClass.getDeclaredMethod("decode", Spell.class);
        int methodModifiers = translate.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }
}
