package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class AgileAdventurer extends Adventurer {

    public AgileAdventurer(Guild guild) {
        super();
        this.name = "Agile";
        this.guild = guild;
    }

    @Override
    public void update() {
        // Accept all quests except 'Escort' quests
        if (!this.guild.getQuestType().equals("Escort"))
            getQuests().add(this.guild.getQuest());
    }
}
