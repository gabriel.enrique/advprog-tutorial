package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class KnightAdventurer extends Adventurer {

    public KnightAdventurer(Guild guild) {
        super();
        this.name = "Knight";
        this.guild = guild;
    }

    @Override
    public void update() {
        // Accept all quests
        getQuests().add(this.guild.getQuest());
    }
}
