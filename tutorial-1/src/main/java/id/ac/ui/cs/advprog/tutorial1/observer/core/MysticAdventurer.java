package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class MysticAdventurer extends Adventurer {

    public MysticAdventurer(Guild guild) {
        super();
        this.name = "Mystic";
        this.guild = guild;
    }

    @Override
    public void update() {
        // Accept all quests except 'Rumble' quests
        if (!this.guild.getQuestType().equals("Rumble"))
            getQuests().add(this.guild.getQuest());
    }
}
