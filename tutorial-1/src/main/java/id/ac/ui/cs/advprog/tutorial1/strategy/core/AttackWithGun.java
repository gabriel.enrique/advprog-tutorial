package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithGun implements AttackBehavior {

    @Override
    public String attack() {
        return "EZ4ENCE ENCE ENCE Dens putted upperbelt Putted upperbelt";
    }

    @Override
    public String getType() {
        return "Gun";
    }
}
