package id.ac.ui.cs.tutorial0.controller;

import id.ac.ui.cs.tutorial0.service.AdventurerCalculatorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class AdventurerController {

    @Autowired
    private AdventurerCalculatorService adventurerCalculatorService;

    @RequestMapping("/adventurer/countPower")
    private String showAdventurerPowerFromBirthYear(@RequestParam("birthYear")int birthYear, Model model) {
        int power = adventurerCalculatorService.countPowerPotensialFromBirthYear(birthYear);

        // Karena keambiguan range class pada soal (tidak dijelaskan inklusif atau eksklusif),
        // saya menggunakan conditional sebagai berikut
        String powerClass = "";
        if (power >= 0 && power <= 20000) {
            powerClass = "C";
        } else if (power > 20000 && power <= 100000) {
            powerClass = "B";
        } else if (power > 100000) {
            powerClass = "A";
        }

        model.addAttribute("power", power);
        model.addAttribute("class", powerClass);
        return "calculator";
    }
}
